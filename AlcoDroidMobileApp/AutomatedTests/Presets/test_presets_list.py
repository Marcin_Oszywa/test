from appium import webdriver
def initiate_driver():
  desired_cap = {
    "deviceName": "4a8ddea30804",
    "platformName": "Android",
    "app": "C:\\Users\\Justyna Oszywa\\Documents\\Marcin\\Tester\\AlcoDroid Alcohol Tracker_v2.37.04.apk",
    "appPackage": "org.M.alcodroid",
    "appActivity": ".AlcoDroidAdSupportedActivity t6594",
    "automationName": "uiautomator2"
  }
  driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_cap)
  driver.implicitly_wait(30)

  return driver

def go_trough_consent(driver):
  driver.find_element_by_id("com.android.packageinstaller:id/permission_allow_button").click()
  driver.find_element_by_id("android:id/button1").click()


def go_to_add_drink(driver):
  driver.find_element_by_id("org.M.alcodroid:id/AddDrinkEntry").click()
  choose_preset_text = driver.find_element_by_id("android:id/action_bar_title").text

  assert choose_preset_text == "Choose a Preset", "We are not on a 'Choose a Preset' screen"

def test_check_presets_list():

  driver = initiate_driver()
  go_trough_consent(driver)
  go_to_add_drink(driver)

  list_of_presets = driver.find_elements_by_xpath("//android.widget.TextView[@resource-id='org.M.alcodroid:id/name']")

  expected_list = ['Light beer', 'Regular beer', 'Wine', 'Generic cocktail', 'Shot']
  actual_list = []

  for i in list_of_presets:
    preset = i.get_attribute("text")
    actual_list.append(preset)
  assert expected_list == actual_list, "TC 6 Preset lists don't match"

