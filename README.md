"Project and execution of an Android application’s automated tests."

This is my part of a group project made at the end of a postgraduate studies "Software Tester" at Wyższa Szkoła Bankowa in Gdańsk.
App chosen was "AlcoDroid", a tool to calculate the ammount of alcohol in blood. Tool for automating was Appium, language Python.
